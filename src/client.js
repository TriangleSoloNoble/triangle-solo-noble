import axios from 'axios'

// const server_url = 'http://localhost:3030'
const server_url = 'https://triangle-solo-noble-server.herokuapp.com/'

const fetchHistory = () => {
  return axios.request({
    url: `${server_url}/history`,
    method: 'get'
  })
    .then((res) => {
      const history = res.data

      return history
    })
}

const fetchSolution = (index) => {
  return axios.request({
    url: `${server_url}/solution`,
    method: 'post',
    data: {
      index: index
    },
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  })
    .then((res) => {
      const solution = res.data

      return solution
    })
}

const fetchRules = () => {
  return axios.request({
    url: `${server_url}/rules`,
    method: 'get'
  })
    .then((res) => {
      const rules = res.data

      return rules
    })
}

export default {
  fetchHistory,
  fetchSolution,
  fetchRules
}
